package com.scalable;

import java.util.List;
import java.util.Map;

public interface Aggregator {

    abstract Map<String, Integer> topN(List<String> links);
}
