package com.scalable;


import com.scalable.aggregators.SimpleAggregator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * This is the main class responsible for querying google and using css selectors to get the links of the first page.
 * It also use Executors to be able to fetch the main links concurrently.
 */
public class Fetcher {

    /**
     * This is a simple class to actually avoid using something like {@code List<Map<String,String>>}, that is a list
     * of mapping a page url to its containing scripts.
     */
    private static class PageScripts {
        String page;
        List<String> scripts;

        PageScripts(String page, List<String> scripts) {
            this.page = page;
            this.scripts = scripts;
        }

        @Override
        public String toString() {
            return String.format("page=%s, scripts=%s", page, scripts);
        }
    }

    /**
     * The term we are looking for.
     */
    private final String term;

    /**
     * The {@code Aggregator} to be used.
     */
    private final Aggregator aggregator;

    /**
     * The executor used to aggregate the Futures on {@code completionService}.
     */
    private final ExecutorService executor;

    /**
     * Completion service itself.
     */
    private final CompletionService<PageScripts> completionService;

    Fetcher(final String term, Aggregator aggregator) {
        this.term = term;
        this.aggregator = aggregator;
        this.executor = Executors.newCachedThreadPool();
        this.completionService = new ExecutorCompletionService<>(executor);
    }


    /**
     * This should be submitted to to the {@code CompletionService} in order to load the target page then return
     * the list of scripts ending in '.js'.
     * @param link to fetch the list of scripts
     * @return the PageScripts object
     */
    private PageScripts getScriptsFromLink(final String link) {
        List<String> scripts = new ArrayList<>();
        try {

            final Document linkedPage = Jsoup.connect(link).get();
            Elements scriptElements = linkedPage.select("script[src$=.js]");


            for (Element e : scriptElements) {
                scripts.add(e.attr("src"));
            }

            return new PageScripts(link, scripts);
        } catch (IOException e) {
            e.printStackTrace();
            // will just ignore this link and return it with an empty list of scripts
        }
        return new PageScripts(link, scripts);
    }

    /**
     * This is where the parallelism magic goes on. It uses a {@code CompletionService} to aggregate all {@code Future}
     * return for each page into a single {@code Future} that should yield the list of scripts from all pages
     * @param links to be fetched.
     * @return a future list of {@code PageScripts}
     */
    private Future<List<PageScripts>> getAllScripts(List<String> links) {
        final List<PageScripts> results = new ArrayList<>();
        return executor.submit(() -> {

            int size = links.size();
            int completed = 0;

            for (final String link : links) {
                completionService.submit(() -> getScriptsFromLink(link));
            }

            while (completed < size) {
                Future<PageScripts> done = completionService.take();
                results.add(done.get());
                completed++;
                System.out.println("Fetch completed for: " + done.get().page);
            }

            return results;

        });
    }

    /**
     * As the main request loop returns a list of {@PageScripts} this method simply collects all the results in a single
     * list.
     * @param pageScripts
     * @return
     */
    private List<String> flatten(List<PageScripts> pageScripts) {
        List<String> finalList = new ArrayList<>();
        for (PageScripts p : pageScripts) {
            finalList.addAll(p.scripts);
        }
        return finalList;
    }


    /**
     * For the google first page, gets only what is considered main link results (no ads or deep intermediate results
     * from pages).
     * @param page supposed to be the first page of a google result
     * @return list of urls as simple {@code Strings}
     */
    private List<String> extractLinks(Document page) {
        final Elements linkElements = page.select("div.rc > h3.r > a[href]");
        List<String> links = new ArrayList<>();

        for (Element element : linkElements) {
            links.add(element.attr("href"));
        }

        return links;
    }


    /**
     * Executes the business logic.
     * @throws IOException
     * @throws ExecutionException
     * @throws InterruptedException
     * @throws TimeoutException
     */
    public void run() throws IOException, ExecutionException, InterruptedException, TimeoutException {

        Document page = Jsoup.connect(String.format("https://google.com/search?q=%s", term)).get();
        List<String> links = extractLinks(page);

        List<PageScripts> results = getAllScripts(links).get(8, TimeUnit.SECONDS);
        List<String> scripts = flatten(results);

        Map<String, Integer> countedBy = aggregator.topN(scripts);

        System.out.println(String.format("[%s] Here the top 5 scripts: %s ", aggregator, countedBy));

        executor.shutdown();

    }

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {

        String term;

        if (args.length == 0) {
            term = "germany";
        } else {
            term = args[0];
        }

        Fetcher fetcher = new Fetcher(term, new SimpleAggregator());

        try {

            fetcher.run();

        } catch (TimeoutException e) {
            System.out.println("Wow, it took too much to getAllScripts the results. I'm really sorry. Please try again.");
        }
    }


}