package com.scalable.aggregators;

import com.scalable.Aggregator;

import java.util.*;
import java.util.stream.Collectors;

public class SimpleAggregator implements Aggregator {

    static final int DEFAULT_TOP = 5;

    private final int topN;

    public SimpleAggregator(int topN) {
        this.topN = topN;
    }

    public SimpleAggregator() {
        this(DEFAULT_TOP);
    }

    private List<String> sanitize(List<String> links) {
        List<String> sanitized = new ArrayList<>();
        for (String link : links) {
            String[] bySlash = link.split("/");
            sanitized.add(bySlash[bySlash.length - 1]);
        }
        return sanitized;
    }

    @Override
    public Map<String, Integer> topN(List<String> links) {
        Map<String, Integer> result = new HashMap<>();
        List<String> sanitized = sanitize(links);

        // TODO: can be replaced by streams + collectors as well.
        for (String link : sanitized) {
            result.computeIfPresent(link, (k, x) -> x + 1);
            result.putIfAbsent(link, 1);
        }

        Map<String, Integer> collected = result.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.naturalOrder()))
                .limit(topN).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));

        return collected;
    }

    @Override
    public String toString() {
        return "SimpleAgregator";
    }
}
