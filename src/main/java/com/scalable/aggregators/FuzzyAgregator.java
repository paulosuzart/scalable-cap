package com.scalable.aggregators;

import com.scalable.Aggregator;

import java.util.List;
import java.util.Map;

/**
 * A possible implementation that would thread for example {@code modernizr.custom.73407.js} as {@code modernizr.js}
 * bringing a more accurate answer. To me implemented.
 */
public class FuzzyAgregator implements Aggregator {

    public FuzzyAgregator() {

    }

    @Override
    public Map<String, Integer> topN(List<String> links) {
        throw new UnsupportedOperationException("Implementation not available.");
    }
}
