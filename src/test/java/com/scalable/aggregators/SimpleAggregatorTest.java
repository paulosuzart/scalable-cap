package com.scalable.aggregators;

import com.scalable.Aggregator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Map;

class SimpleAggregatorTest {


    @Test
    @DisplayName("Test script frequency")
    void simpleTest() {
        Aggregator aggregator = new SimpleAggregator();

        Map<String, Integer> result = aggregator.topN(new ArrayList<String>() {{
            add("1.js");
            add("2.js");
            add("1.js");
            add("2.js");
            add("3.js");
        }});

        assertEquals(3, result.size());
        assertEquals(2, result.get("1.js").intValue());


    }


}
